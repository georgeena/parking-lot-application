 
var helloApp = angular.module("helloApp", []);
helloApp.controller("VehicleCtrl", function($scope) {
$scope.vehicles = 
[
{ 'state':'MA',
'plate': 'SBA 1234A',
'time':12,
'type': 'Valet'

},
{'state':'IL',
'plate': 'ALE 4577',
'time':23,
'type': 'Valet'
}
];

refreshChart();

$scope.addRow = function()
{


if($scope.type ==='Term')
{	 

$('#termCount').val( function(i, oldval) {
return ++oldval;
});	
} else if($scope.type ==='Valet') {

$('#valetCount').val( function(i, oldval) {
return ++oldval;
});
} 
else
{

$('#regularCount').val( function(i, oldval) {
return ++oldval;
});
}
$('#remainingCount').val( function(i, oldval) {
return --oldval;
});

refreshChart();


$scope.vehicles.push({'state':$scope.state,'plate':$scope.plate, 'time':getTime(),'type':$scope.type});
$scope.state='';
$scope.plate='';
$scope.time='';
$scope.type='';
};

$scope.removeRow = function(plate){
var index = -1;
if(confirm('Do you really want to delete ?'))
{

var comArr = eval( $scope.vehicles );
for( var i = 0; i < comArr.length; i++ ) {
		if( comArr[i].plate === plate ) {
		index = i;
		break;
		}
		}

		if(comArr[index].type ==='Term')
		{	 
		$('#termCount').val( function(i, oldval) {
		return --oldval;
		});	
		} 
		else if(comArr[index].type ==='Valet') 
		{

		$('#valetCount').val( function(i, oldval) {
		return --oldval;
		});
		} 
		else
		{

		$('#regularCount').val( function(i, oldval) {
		return --oldval;
		});
		}
		$('#remainingCount').val( function(i, oldval) {
		return ++oldval;
		});

		refreshChart();

		if( index === -1 ) {
		//alert( "Something gone wrong" );
		}
		$scope.vehicles.splice( index, 1 );
}};
		});  

		function refreshChart()
		{
		FusionCharts.ready(function() {
		var revenueChart = new FusionCharts({
		type : "pie2d",
		renderAt : "chartContainer",
		width : "500",
		height : "300",
		dataFormat : "json",
		dataSource : {
		"chart" : {
		"caption" : "Parking Lot Details",					
		"xAxisName" : "Type",
		"yAxisName" : "Count",
		"theme" : "zune"
		},
		"data" : [ {
		"label" : "Regular",
		"value" : $('#regularCount').val()
		}, 
		{
		"label" : "Term",
		"value" : $('#termCount').val()
		},
		{
		"label" : "Valet",
		"value" : $('#valetCount').val()
		},
		{

		"label" : "Remaining Space",
		"value" : $('#remainingCount').val()

		}

		]
		}

		});
		revenueChart.render("chartContainer");
		});
		}
		